mod polyterm;
mod ops;
mod polynomial;

#[cfg(test)]
mod tests {

    use super::ops::*;
    use super::polyterm::*;
    use super::polynomial::*;
    #[test]
    fn create_polyterm() {
        assert_eq!(format!("{}",PolyTerm::new(12,12)),"12x^12");
    }
    #[test]
    fn imul_tst() {
        let mut term = PolyTerm::new(2,3);
        term.imul(18);
        assert_eq!(format!("{}",term),"36x^3")
    }
    #[test]
    fn tmul_tst() {
        let mut term = PolyTerm::new(2,3);
        term.tmul(term);
        assert_eq!(format!("{}",term),"4x^6")
    }
    #[test]
    fn tadd_tst() {
        let mut term = PolyTerm::new(3,4);
        let term2 = PolyTerm::new(6,4);
        term.tadd(term2);
        assert_eq!(format!("{}",term),"9x^4");

    }
    #[test]
    fn polynomial() {
        let mut term = Polynomial::new().with(3,4).with(-3,3).with(-5,2);
        assert_eq!(format!("{}",term),"+3x^4-3x^3-5x^2");
        let mut term2 = Polynomial::new().with(3,4);
        term2.imul(2);
        assert_eq!(format!("{}",term2),"+6x^4");
        term.tmul(PolyTerm::new(3,4));
        assert_eq!(format!("{}",term),"+9x^8-9x^7-15x^6");

        //padd1
        let mut term = Polynomial::new().with(5,1).with(2,0);
        let mut term2 = Polynomial::new().with(6,1).with(2,0);
        term.padd(term2);
        assert_eq!(format!("{}",term),"+11x^1+4x^0");

        //tadd
        let mut term = Polynomial::new().with(5,1).with(2,0);
        let mut term2 = PolyTerm::new(4,0);
        term.tadd(term2);
        assert_eq!(format!("{}",term),"+5x^1+6x^0");


        //pmul
        let mut term = Polynomial::new().with(5,1).with(2,0);
        let mut term2 = Polynomial::new().with(6,1).with(2,0);
        term.pmul(term2);
        assert_eq!(format!("{}",term),"+30x^2+22x^1+4x^0");
            
    }
}
