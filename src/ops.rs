use super::polyterm::PolyTerm;
use super::polynomial::Polynomial;

pub trait Imul {
    fn imul(&mut self,v:i128);
}

pub trait Tmul {
    fn tmul(&mut self,v:PolyTerm);
}

pub trait Tadd {
    fn tadd(&mut self,v:PolyTerm);
}
pub trait Pmul {
    fn pmul(&mut self,v:Polynomial);
}
pub trait Padd {
    fn padd(&mut self,v:Polynomial);
}
