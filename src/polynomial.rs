use super::polyterm::PolyTerm;
use std::fmt;
use super::ops::*;

#[derive(Clone,Default)]
pub struct Polynomial {
    pub terms: Vec<PolyTerm>,
    
}


impl Polynomial {
    pub fn new() -> Self {
        Default::default()
    }
    pub fn with(mut self, coef:i128,exp:i128) -> Self {
        self.terms.push(PolyTerm::new(coef,exp));
        self
    }
}

impl fmt::Display for Polynomial {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut ret = String::default();
        for term in self.terms.iter() {
            let mut tmp = String::default();
            if term.coef > 0 {
                tmp = format!("+{}",term);
            } else {
                tmp = format!("{}",term);
            }
            ret = format!("{}{}",ret,tmp);
        }
        write!(f,"{}",ret)
    }

}

impl Imul for Polynomial {
    fn imul(&mut self,v:i128) {
        for term in self.terms.iter_mut() {
            term.imul(v);
        }
    }
}

impl Tmul for Polynomial {
    fn tmul(&mut self,v:PolyTerm) {
        for term in self.terms.iter_mut() {
            term.tmul(v)
        }
    }
}

impl Tadd for Polynomial {
    fn tadd(&mut self,v:PolyTerm) {
        let mut done = false;
        for term in self.terms.iter_mut() {
            if done == true {
                panic!();
            }
            if term.exp == v.exp {
                term.coef+=v.coef;
                done = true;
                break;
            }
        }
    }
}


impl Padd for Polynomial {
    fn padd(&mut self,v:Polynomial)  {
        for term in v.terms.iter() {
            self.tadd(*term);
        }
    }
}

//fixme: too many clones
impl Pmul for Polynomial {
    fn pmul(&mut self,v:Polynomial) {
        let mut resf: Vec<Polynomial> = vec!();
        for &term in v.terms.iter() {
            let mut slf = self.clone();
            slf.tmul(term);
            resf.push(slf)
        }
        let mut t1 = resf[0].clone();
        resf.remove(0);
        for term in resf.iter() {
            t1.padd(term.clone())
        }
        //mem::swap here??
        *self = t1
    }
}
