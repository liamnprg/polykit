
use crate::polynomial::Polynomial;
use crate::ops::*;
use std::fmt;

#[derive(Copy,Clone)]
pub struct PolyTerm {
    pub coef: i128,
    pub exp: i128,
}

impl PolyTerm {
    pub fn new(coef:i128,exp:i128) -> Self {
        PolyTerm {
            coef:coef,
            exp:exp,
        }
    }
}

impl fmt::Display for PolyTerm {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}x^{}", self.coef, self.exp)
    }
}

impl Imul for PolyTerm {
    fn imul(&mut self,v:i128) {
        self.coef=self.coef*v;
    }
}
impl Tmul for PolyTerm {
    fn tmul(&mut self,v:PolyTerm) {
        self.coef=v.coef*self.coef;
        self.exp=v.exp+self.exp;
    }
}

impl Tadd for PolyTerm {
    fn tadd(&mut self,v:PolyTerm) {
        assert_eq!(self.exp,v.exp);
        self.coef=v.coef+self.coef;
    }
}
